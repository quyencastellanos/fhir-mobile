//
//  MobileFHIR.h
//  MobileFHIR
//
//  Created by Quyen Castellanos on 7/18/17.
//  Copyright © 2017 Quyen Castellanos. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for MobileFHIR.
FOUNDATION_EXPORT double MobileFHIRVersionNumber;

//! Project version string for MobileFHIR.
FOUNDATION_EXPORT const unsigned char MobileFHIRVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <MobileFHIR/PublicHeader.h>


