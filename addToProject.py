#!/usr/bin/env python
import sys
import os
import glob
from pbxproj import XcodeProject

def addFiles(project, files):
	#open the project

	project = XcodeProject.load(project)

	#add file to it, force=false to not add it if it's already in the project
	for file in os.listdir(files):
		if file.endswith('.swift'):
			project.add_file(os.path.join(files, file), force = False)
	project.save()

def main():
	if(len(sys.argv) < 3):
		print 'incorrect amount of arguments'
		exit(1)
	addFiles(sys.argv[1], sys.argv[2])
	exit()

if __name__ == "__main__":
    main()